package fr.epita.quizmgr.Launcher;

import java.util.Scanner;
import fr.epita.quizmgr.utility.QuizMgrUtility;

/**
 * @author KadySwar
 * @category File containing main method which is to be invoked to access the
 *           Quiz Manager Application
 */
public class Launcher {

	private static final String UNAME = "ADMIN";
	private static final String PWD = "PWD";

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(" ********** Welcome to my application !  ********** ");
		System.out.println(" ********** Kindly enter your details !  ********** ");
		String username = getResp(scanner, "User Name :");
		/*
		 * Based on the username entered the flow divided into two , (1) towards admin
		 * console to perform CRUD on quiz question & answers (2) towards student
		 * console who can take up the test and view the results
		 */
		if (UNAME.equals(username)) {
			adminLogin(scanner);
		} else {
			studentLogin(scanner);
		}
		scanner.close();
	}

	/**
	 * @param scanner Upon Authentication of admin, provides CRUD operation
	 *                accessibility based on user input. QuizMgrUtility is used to
	 *                ensure the main method looks neat for better readability
	 * 
	 *                Based on numerical key in
	 * 
	 *                Exits upon confirmation
	 */
	private static void adminLogin(Scanner scanner) {
		QuizMgrUtility qmUtil = new QuizMgrUtility();
		String password = getResp(scanner, "Password : ");
		if (authenticateAdmin(password)) {
			boolean exit = false;
			do {
				System.out.println(" ********** Welcome Admin ********** ");
				System.out.println("Please choose an action\n1.Create \n2.Search \n3.Update \n4.Delete \n5.Exit \n");
				String option = getResp(scanner, "Please input ur choice 1|2|3|4|5");

				switch (option) {
				case "1":
					qmUtil.createQuestionAnswers(scanner);
					break;
				case "2":
					qmUtil.searchQuestionAns(scanner);
					break;
				case "3":
					qmUtil.updateQuestion(scanner);
					break;
				case "4":
					qmUtil.deleteQuestion(scanner);
					break;
				case "5":
					option = getResp(scanner, "Do you confirm to exit ?! ");
					exit = option.equalsIgnoreCase("Y");
					System.out.println(" ********** Exiting application ! ********** ");
				default:
					break;
				}
			} while (!exit);
		} else {
			System.out.println("!!! Invalid credentials, exiting application  ********** ");
		}
	}

	/**
	 * QuizMgrUtility is called to start quiz session from here
	 */
	private static void studentLogin(Scanner scanner) {
		QuizMgrUtility qmUtil = new QuizMgrUtility();
		int score = qmUtil.quizSession(scanner);
		System.out.println("Your score is : " + score);
	}

	/**
	 * @param password || auntheticates admin login
	 */
	private static boolean authenticateAdmin(String password) {
		if (PWD.equals(password)) {
			return true;
		}
		return false;
	}

	/**
	 * @param scanner
	 * @param proposedQuestion
	 * @return user entered input from the console
	 * 
	 *         Utility since the same process is repeated often throughout the class
	 */
	private static String getResp(Scanner scanner, String proposedQuestion) {
		System.out.println("Kindly enter the " + proposedQuestion);
		return scanner.nextLine();
	}
}

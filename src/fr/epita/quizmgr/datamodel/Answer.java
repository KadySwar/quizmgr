package fr.epita.quizmgr.datamodel;

/**
 * @author KadySwar
 * @category data model for Answer table for open questions, mapper with
 *           respective q_id(primarykey of question table)
 */
public class Answer {
	private int answerID;
	private int questionID; //foreign key to link with question table
	private String answer;

	public int getAnswerID() {
		return answerID;
	}

	public void setAnswerID(int answerID) {
		this.answerID = answerID;
	}

	public int getQuestionID() {
		return questionID;
	}

	public void setQuestionID(int questionID) {
		this.questionID = questionID;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@Override
	public String toString() {
		return "[answerID=" + answerID + ", answer=" + answer + "]";
	}

}

package fr.epita.quizmgr.datamodel;

/**
 * @author KadySwar
 * @category datamodel for MultipleChoice Answers
 *
 */
public class MCA extends Answer {

	private boolean isCorrectOption;//provides information is its valid choice

	public boolean isCorrectOption() {
		return isCorrectOption;
	}

	public void setCorrectOption(boolean isCorrectOption) {
		this.isCorrectOption = isCorrectOption;
	}

	@Override
	public String toString() {
		return "[isCorrectOption=" + isCorrectOption + "," + super.toString() + "]";
	}

}

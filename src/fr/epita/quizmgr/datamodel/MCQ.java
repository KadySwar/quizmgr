package fr.epita.quizmgr.datamodel;

import java.util.List;

/**
 * @author KadySwar
 * @category datamodel for Multiple Choice questions
 *
 */
public class MCQ extends Question {
	private List<MCA> answerList; // includes AnswerList for respect MCQ

	public List<MCA> getAnswerList() {
		return answerList;
	}

	public void setAnswerList(List<MCA> answerList) {
		this.answerList = answerList;
	}

}

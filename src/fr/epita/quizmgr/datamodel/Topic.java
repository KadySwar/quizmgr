package fr.epita.quizmgr.datamodel;

/**
 * @author KadySwar
 * @category dataodel for topic table
 */
public class Topic {
	private int topicID; // primary key of topic table
	private String topicName;

	public int getTopicID() {
		return topicID;
	}

	public void setTopicID(int topicID) {
		this.topicID = topicID;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	@Override
	public String toString() {
		return " TopicName=" + topicName;
	}

}

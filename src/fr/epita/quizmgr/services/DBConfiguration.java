package fr.epita.quizmgr.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class DBConfiguration {

	private Properties properties;

	private static DBConfiguration instance; // static instance to ensure only one object is created per application : Singleton

	/**
	 * private constructor to implement singleton concept
	 */
	private DBConfiguration() {
		File file = new File("app.properties");//config file provided with the project
		properties = new Properties();
		try {
			properties.load(new FileInputStream(file));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @return singleton instance of the desired config obj
	 */
	public static DBConfiguration getInstance() {
		if (instance == null) {
			instance = new DBConfiguration();
		}
		return instance;

	}

	/**
	 * @param configKey
	 * @return respective value for the requested property
	 */
	public String getConfigValue(String configKey) {
		return properties.getProperty(configKey);
	}
}

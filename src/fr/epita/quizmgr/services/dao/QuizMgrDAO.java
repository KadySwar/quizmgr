package fr.epita.quizmgr.services.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.epita.quizmgr.datamodel.Answer;
import fr.epita.quizmgr.datamodel.MCA;
import fr.epita.quizmgr.datamodel.MCQ;
import fr.epita.quizmgr.datamodel.Question;
import fr.epita.quizmgr.datamodel.Topic;
import fr.epita.quizmgr.services.DBConfiguration;

/**
 * @author KadySwar 
 * 			
 * 			Initially DBConfiguration is got an a singleton instance and
 *         db details are fetched from properties file Queries have been made
 *         generic for re-usability when needed Based on questions instance
 *         (Question or MCQ), table choice is made advanced try is used so the
 *         connection & prepared statements get closed automatically
 */
public class QuizMgrDAO {

	private static final String INSERT_QUERY_Q = "insert into tableName (QUESTION,DIFFICULTY,TOPIC_ID) VALUES (?, ?, ?)";
	private static final String UPDATE_QUERY_R = "update tableName set QUESTION=?,DIFFICULTY=?,TOPIC_ID=? WHERE Q_ID=?";
	private static final String DELETE_QUERY_R = "delete tableName WHERE Q_ID=?";
	private static final String SEARCH_BY_QUESTION_QUERY = "select Q_ID,QUESTION,DIFFICULTY,T_ID,TNAME from tableName q,TOPIC tp where q.TOPIC_ID=tp.T_ID and QUESTION LIKE ?";
	private static final String SEARCH_BY_TOPIC_QUERY = "select Q_ID,QUESTION,DIFFICULTY,T_ID,TNAME from tableName q,TOPIC tp where q.TOPIC_ID=tp.T_ID and tp.TNAME like ?";
	private static final String SEARCH_BY_ID_QUERY = "select Q_ID,QUESTION,DIFFICULTY,T_ID,TNAME from tableName q,TOPIC tp where q.TOPIC_ID=tp.T_ID and Q_ID= ?";
	private static final String SEARCH_ANSWER = "select A_ID,ANSWER from ANSWER where Q_ID= ?";
	private static final String SEARCH_MCA = "select A_ID,CHOICES,ISCORRECTANS from MCA where Q_ID= ?";
	private static final String GET_QUIZ_QUESTIONS = "select Q_ID,QUESTION,DIFFICULTY,T_ID,TNAME from tableName q,TOPIC tp where q.TOPIC_ID=tp.T_ID and DIFFICULTY= ?";

	private Connection getConnection() throws SQLException {

		DBConfiguration config = DBConfiguration.getInstance();

		String url = config.getConfigValue("jdbc.url");
		String username = config.getConfigValue("jdbc.username");
		String password = config.getConfigValue("jdbc.password");

		return DriverManager.getConnection(url, username, password);
	}

	public Question searchByID(Question question) {
		String queryString = new String();
		Question currentQuestion;
		if (question instanceof MCQ) {
			currentQuestion = new MCQ();
			queryString = SEARCH_BY_ID_QUERY.replace("tableName", "MCQ");
		} else {
			currentQuestion = new Question();
			queryString = SEARCH_BY_ID_QUERY.replace("tableName", "Question");
		}
		try (Connection connection = getConnection();
				PreparedStatement readStmt = connection.prepareStatement(queryString);) {
			readStmt.setInt(1, question.getQuestionID());
			ResultSet rs = readStmt.executeQuery();
			while (rs.next()) {
				int qId = rs.getInt(1);
				String questionLabel = rs.getString(2);
				int difficulty = rs.getInt(3);
				int topicID = rs.getInt(4);
				currentQuestion.setQuestionID(qId);
				currentQuestion.setQuestion(questionLabel);
				currentQuestion.setDifficulty(difficulty);
				currentQuestion.setTopicID(topicID);
				Topic tpic = new Topic();
				String tname = rs.getString(5);
				tpic.setTopicName(tname);
				currentQuestion.setTopic(tpic);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return currentQuestion;
	}

	public List<Question> searchByTopic(Question question) {
		String queryString = new String();
		List<Question> questions = new ArrayList<>();
		if (question instanceof MCQ) {
			queryString = SEARCH_BY_TOPIC_QUERY.replace("tableName", "MCQ");
		} else {
			queryString = SEARCH_BY_TOPIC_QUERY.replace("tableName", "Question");
		}
		try (Connection connection = getConnection();
				PreparedStatement readStmt = connection.prepareStatement(queryString);) {
			String topicName = "Java";
			if (question.getTopicID() == 2) {
				topicName = "UML";
			}
			readStmt.setString(1, topicName);
			ResultSet rs = readStmt.executeQuery();
			while (rs.next()) {
				int qId = rs.getInt(1);
				String questionLabel = rs.getString(2);
				int difficulty = rs.getInt(3);
				int topicID = rs.getInt(4);
				Question currentQuestion = new Question();
				currentQuestion.setQuestionID(qId);
				currentQuestion.setQuestion(questionLabel);
				currentQuestion.setDifficulty(difficulty);
				currentQuestion.setTopicID(topicID);
				Topic tpic = new Topic();
				String tname = rs.getString(5);
				tpic.setTopicName(tname);
				currentQuestion.setTopic(tpic);
				questions.add(currentQuestion);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return questions;
	}

	public List searchByQuestion(Question question) {
		List<Question> questions = new ArrayList<>();
		String queryString = new String();

		if (question instanceof MCQ) {
			queryString = SEARCH_BY_QUESTION_QUERY.replace("tableName", "MCQ");
		} else {
			queryString = SEARCH_BY_QUESTION_QUERY.replace("tableName", "Question");
		}

		try (Connection connection = getConnection();
				PreparedStatement readStmt = connection.prepareStatement(queryString);) {
			readStmt.setString(1, "%" + question.getQuestion() + "%");
			ResultSet rs = readStmt.executeQuery();
			while (rs.next()) {
				int qid = rs.getInt(1);
				String questionLabel = rs.getString(2);
				int difficulty = rs.getInt(3);
				int topicID = rs.getInt(4);
				String tname = rs.getString(5);
				Question currentQuestion = new Question();
				currentQuestion.setQuestionID(qid);
				currentQuestion.setQuestion(questionLabel);
				currentQuestion.setDifficulty(difficulty);
				currentQuestion.setTopicID(topicID);
				Topic tpic = new Topic();
				tpic.setTopicName(tname);
				currentQuestion.setTopic(tpic);
				questions.add(currentQuestion);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return questions;
	}

	public int createQuestion(Question question, MCQ mcq) {
		String queryString = new String();
		if (question instanceof MCQ) {
			queryString = INSERT_QUERY_Q.replace("tableName", "MCQ");
		} else {
			queryString = INSERT_QUERY_Q.replace("tableName", "Question");
		}
		int updateCount = 0;
		try (Connection connection = getConnection();
				PreparedStatement createStmt = connection.prepareStatement(queryString);) {
			if (mcq != null) {

				createStmt.setString(1, mcq.getQuestion());
				createStmt.setInt(2, mcq.getDifficulty());
				createStmt.setInt(3, mcq.getTopicID());
				updateCount = createStmt.executeUpdate();
			} else {
				createStmt.setString(1, question.getQuestion());
				createStmt.setInt(2, question.getDifficulty());
				createStmt.setInt(3, question.getTopicID());
				updateCount = createStmt.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return updateCount;
	}

	public void createAnswerForOpenQuestion(Answer answer) {
		try (Connection connection = getConnection();
				PreparedStatement createAnswerStmt = connection
						.prepareStatement("insert into ANSWER (Q_ID,ANSWER) values (?,?)");) {
			createAnswerStmt.setString(2, answer.getAnswer());
			createAnswerStmt.setInt(1, answer.getQuestionID());
			createAnswerStmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void createAnswerForMCQ(List<MCA> multipleAnswersList) {
		try (Connection connection = getConnection();
				PreparedStatement createAnswerStmt = connection
						.prepareStatement("insert into MCA (Q_ID,CHOICES,ISCORRECTANS) values (?,?,?)");) {
			for (MCA answer : multipleAnswersList) {
				createAnswerStmt.setString(2, answer.getAnswer());
				createAnswerStmt.setInt(1, answer.getQuestionID());
				createAnswerStmt.setBoolean(3, answer.isCorrectOption());
				createAnswerStmt.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateQuestion(Question question) {
		String queryString = new String();
		if (question instanceof MCQ) {
			queryString = UPDATE_QUERY_R.replace("tableName", "MCQ");
		} else {
			queryString = UPDATE_QUERY_R.replace("tableName", "Question");
		}
		try (Connection connection = getConnection();
				PreparedStatement updateStmt = connection.prepareStatement(queryString);) {
			updateStmt.setString(1, question.getQuestion());
			updateStmt.setInt(2, question.getDifficulty());
			updateStmt.setInt(3, question.getTopicID());
			updateStmt.setInt(4, question.getQuestionID());
			updateStmt.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteQuestionAns(Question question) {
		try (Connection connection = getConnection();) {
			PreparedStatement delStmt, delAnsStmnt;
			if (question instanceof MCQ) {
				delStmt = connection.prepareStatement(DELETE_QUERY_R.replace("tableName", "MCQ"));
				delAnsStmnt = connection.prepareStatement("delete MCA where Q_ID= ?");
			} else {
				delStmt = connection.prepareStatement(DELETE_QUERY_R.replace("tableName", "Question"));
				delAnsStmnt = connection.prepareStatement("delete ANSWER where Q_ID= ?");
			}
			delAnsStmnt.setInt(1, question.getQuestionID());
			delStmt.setInt(1, question.getQuestionID());
			int executeUpdate = delAnsStmnt.executeUpdate();
			if (executeUpdate > 0) {
				delAnsStmnt.close();
				delStmt.execute();
			}
			delStmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Answer searchAnswer(int qID) {
		Answer answer = new Answer();
		try (Connection connection = getConnection();
				PreparedStatement readStmt = connection.prepareStatement(SEARCH_ANSWER);) {

			readStmt.setInt(1, qID);
			ResultSet rs = readStmt.executeQuery();
			while (rs.next()) {
				int aId = rs.getInt(1);
				String answerLabel = rs.getString(2);
				answer.setAnswerID(aId);
				answer.setAnswer(answerLabel);
				answer.setQuestionID(qID);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return answer;
	}

	public List<MCA> searchMCA(int qID) {
		List<MCA> mcaList = new ArrayList<MCA>();
		try (Connection connection = getConnection();
				PreparedStatement readAnsStmt = connection.prepareStatement(SEARCH_MCA);) {
			readAnsStmt.setInt(1, qID);
			ResultSet rs = readAnsStmt.executeQuery();
			while (rs.next()) {
				int aId = rs.getInt(1);
				String answerLabel = rs.getString(2);
				boolean isCrctOpt = rs.getBoolean(3);
				MCA currentAnswer = new MCA();
				currentAnswer.setAnswer(answerLabel);
				currentAnswer.setAnswerID(aId);
				currentAnswer.setQuestionID(qID);
				currentAnswer.setCorrectOption(isCrctOpt);
				mcaList.add(currentAnswer);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mcaList;
	}

	public List getQuestionsForQuiz(Question question) {

		List questions = new ArrayList<>();
		String queryString = new String();
		Question currentQuestion;
		if (question instanceof MCQ) {
			queryString = GET_QUIZ_QUESTIONS.replace("tableName", "MCQ");
			currentQuestion = new MCQ();
		} else {
			queryString = GET_QUIZ_QUESTIONS.replace("tableName", "Question");
			currentQuestion = new Question();
		}
		try (Connection connection = getConnection();
				PreparedStatement readStmt = connection.prepareStatement(queryString);) {

			readStmt.setInt(1, question.getDifficulty());
			ResultSet rs = readStmt.executeQuery();
			while (rs.next()) {
				int qid = rs.getInt(1);
				String questionLabel = rs.getString(2);
				int difficulty = rs.getInt(3);
				int topicID = rs.getInt(4);
				String tname = rs.getString(5);
				currentQuestion.setQuestionID(qid);
				currentQuestion.setQuestion(questionLabel);
				currentQuestion.setDifficulty(difficulty);
				currentQuestion.setTopicID(topicID);
				Topic tpic = new Topic();
				tpic.setTopicName(tname);
				currentQuestion.setTopic(tpic);
				questions.add(currentQuestion);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return questions;
	}
}

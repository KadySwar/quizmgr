package fr.epita.quizmgr.utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import fr.epita.quizmgr.datamodel.Answer;
import fr.epita.quizmgr.datamodel.MCA;
import fr.epita.quizmgr.datamodel.MCQ;
import fr.epita.quizmgr.datamodel.Question;
import fr.epita.quizmgr.datamodel.Topic;
import fr.epita.quizmgr.services.dao.QuizMgrDAO;


/**
 * The purpose of this class is handle launcher class methods.
 * @author KadySwar
 * @category Helper Class - used for better readability. 
 */
public class QuizMgrUtility {

	//creating DAO object as class variable which will be used through the class.
	private static QuizMgrDAO daoObj = new QuizMgrDAO();

	
	/**
	 * This method is to create questions into the question table.
	 * @param scanner - used to get inputs from console.
	 * 
	 */
	public void createQuestionAnswers(Scanner scanner) {

		int tableChoice = Integer.parseInt(getResp(scanner, "type of question: \n1.Open Question \n2.MCQ"));
		String questionLabel = getResp(scanner, "Question : ");
		int difficulty = Integer.parseInt(getResp(scanner, "Difficulty : \n1.Simple \n2.Medium \n3.High "));
		int topicID = Integer.parseInt(getResp(scanner, " TopicID: \n1.Java \n2.UML"));
		if (tableChoice == 1) {
			createOpenQuestion(scanner, daoObj, questionLabel, difficulty, topicID);
		} else if (tableChoice == 2) {
			createMCQ(scanner, daoObj, questionLabel, difficulty, topicID);
		}
	}
	
	
	/**
	 * Private method to create Multiple Choice Questions into MCQ table which is invoked by createQuestionAnswers.
	 * <p>n number of options for a questions can be given.
	 * </p>
	 * @param scanner - Object to get inputs from console
	 * @param daoObj - DAO object
	 * @param questionLabel - Question string
	 * @param difficulty - Difficult level (int)
	 * @param topicID - topic primary ID (int)
	 */
	private void createMCQ(Scanner scanner, QuizMgrDAO daoObj, String questionLabel, int difficulty, int topicID) {
		int updateCount;
		MCQ mcq = new MCQ();
		mcq.setDifficulty(difficulty);
		mcq.setQuestion(questionLabel);
		mcq.setTopicID(topicID);
		List<MCA> multipleAnswersList = new ArrayList<>();
		boolean exitChoice = false;
		do {
			MCA mca = new MCA();
			mca.setAnswer(getResp(scanner, "Answer choice : "));
			mca.setCorrectOption("Y".equalsIgnoreCase(getResp(scanner, "Is this a correct answer:Y/N")) ? true : false);
			multipleAnswersList.add(mca);
			exitChoice = "Q".equalsIgnoreCase(getResp(scanner, "Press q to exit or any key to continue !")) ? true
					: false;
		} while (!exitChoice);
		mcq.setAnswerList(multipleAnswersList);
		updateCount = daoObj.createQuestion(null, mcq);

		if (updateCount != 0) {
			List<Question> searchByQuestion = daoObj.searchByQuestion(mcq);
			if (!searchByQuestion.isEmpty()) {
				for (MCA mcaRow : multipleAnswersList) {
					mcaRow.setQuestionID(searchByQuestion.get(0).getQuestionID());
				}
				daoObj.createAnswerForMCQ(multipleAnswersList);
			}

		}
	}

	
	/**
	 * Private method which is invoked by createQuestionAnswers to create Open Questions into QUESTION table.
	 * @param scanner - Object to get inputs from console
	 * @param daoObj - DAO object
	 * @param questionLabel - Question string
	 * @param difficulty - Difficult level (int)
	 * @param topicID - topic primary ID (int)
	 */
	private void createOpenQuestion(Scanner scanner, QuizMgrDAO daoObj, String questionLabel, int difficulty,
			int topicID) {
		int updateCount = 0;
		Question question = new Question();
		question.setDifficulty(difficulty);
		question.setQuestion(questionLabel);
		question.setTopicID(topicID);
		Answer answer = new Answer();
		answer.setAnswer(getResp(scanner, "Answer : "));
		question.setAnswer(answer);
		updateCount = daoObj.createQuestion(question, null);

		if (updateCount != 0) {
			List<Question> searchByQuestion = daoObj.searchByQuestion(question);
			if (!searchByQuestion.isEmpty()) {
				answer.setQuestionID(searchByQuestion.get(0).getQuestionID());
				daoObj.createAnswerForOpenQuestion(answer);
			}

		}
	}
	
	
	/**
	 * Main method: Search Question and answer either by Topic, ID or Keyword
	 * @param scanner
	 */
	public void searchQuestionAns(Scanner scanner) {
		boolean loop = true;
		do {
			String searchBy = getResp(scanner, " Search method by:\n1:Topic \n2.ID \n3.keyword \n4:Exit");
			switch (searchBy) {
			case "1":
				searchQuestionsByTopic(scanner, daoObj);
				break;
			case "2":
				searchByID(scanner, daoObj);
				break;
			case "3":
				searchByKeyword(scanner, daoObj);
				break;
			case "4":
				loop = false;
				break;
			default:
				System.out.println("Wrong Selection. Please try again!");
				break;
			}
		} while (loop);
	}
	

	/**
	 * Sub method of searchQuestionAns
	 * <p>
	 * Searches question in QUESTION and MCQ table
	 * </p>
	 * @param scanner
	 * @param daoObj2 - DAO object to access database connection
	 */
	private void searchByKeyword(Scanner scanner, QuizMgrDAO daoObj2) {
		int tableChoice = Integer.parseInt(getResp(scanner, "type of question: \n1.Open Question \n2.MCQ"));
		Question question;
		if (tableChoice == 1) {
			question = new Question();
		} else {
			question = new MCQ();
		}
		String searchKey = getResp(scanner, " Keyword :");
		question.setQuestion(searchKey);
		List<Question> searchByQuestionList = daoObj.searchByQuestion(question);
		if (!searchByQuestionList.isEmpty()) {
			searchByQuestionList.forEach((questionRow) -> {
				System.out.println(questionRow);
			});
		} else {
			System.out.println("0 record found!");
		}
	}

	/**
	 * Search questions by ID
	 * @param scanner
	 * @param daoObj2
	 */
	private void searchByID(Scanner scanner, QuizMgrDAO daoObj2) {
		int tableChoice = Integer.parseInt(getResp(scanner, "type of question: \n1.Open Question \n2.MCQ"));
		Question question;
		if (tableChoice == 1) {
			question = new Question();
		} else {
			question = new MCQ();
		}
		int searchID = Integer.parseInt(getResp(scanner, " Enter question ID :"));
		question.setQuestionID(searchID);
		Question searchByIDResult = daoObj.searchByID(question);
		if (tableChoice == 1) {
			Answer answer = daoObj.searchAnswer(searchByIDResult.getQuestionID());
			System.out.println("___________________________________\n ");
			System.out.println("Question details : \n\n " + searchByIDResult);
			System.out.println("\n___________________________________\n ");
			System.out.println("Respective answer details : \n " + answer);
		} else {
			List<MCA> mcaList = daoObj.searchMCA(searchID);
			System.out.println("___________________________________\n ");
			System.out.println("Question details : \n\n " + searchByIDResult);
			if (!mcaList.isEmpty()) {
				System.out.println("\n___________________________________ ");
				System.out.println("\nRespective answer details : \n ");
				System.out.println("___________________________________ ");
				mcaList.forEach((mcaRow) -> {
					System.out.println(mcaRow);
				});
			} else {
				System.out.println("0 record found!");
			}
		}
	}

	/**
	 * Search question by Topic 
	 * @param scanner
	 * @param daoObj2
	 */
	private void searchQuestionsByTopic(Scanner scanner, QuizMgrDAO daoObj2) {

		int tableChoice = Integer.parseInt(getResp(scanner, "type of question: \n1.Open Question \n2.MCQ"));
		Question question;
		if (tableChoice == 1) {
			question = new Question();
		} else {
			question = new MCQ();
		}
		int searchTopic = Integer.parseInt(getResp(scanner, " Topic:\n1:Java \n2:UML"));
		Topic topic = new Topic();
		topic.setTopicID(searchTopic);
		question.setTopic(topic);
		question.setTopicID(searchTopic);
		List<Question> searchByQuestionList = daoObj.searchByTopic(question);
		if (!searchByQuestionList.isEmpty()) {
			searchByQuestionList.forEach((questionRow) -> {
				System.out.println(questionRow);
			});
		} else {
			System.out.println("0 record found!");
		}
	}

	/**
	 * This is method for student quiz.
	 * <p>
	 * It will fetch question from both type of questions and based on user input, score will be printed. 
	 * </p> 
	 * @param scanner
	 * @return
	 */
	public int quizSession(Scanner scanner) {
		int score = 0;
		int difficultyChoice = Integer
				.parseInt(getResp(scanner, "difficulty level: \n1.Beginner \n2.Intermediate \n3.Expert"));
		Question questionObj = new Question();
		MCQ mcqObj = new MCQ();
		questionObj.setDifficulty(difficultyChoice);
		mcqObj.setDifficulty(difficultyChoice);
		List<Question> getQuestionsList = daoObj.getQuestionsForQuiz(questionObj);
		List<MCQ> getMCQuestionList = daoObj.getQuestionsForQuiz(mcqObj);

		Collections.shuffle(getQuestionsList, new Random());
		Collections.shuffle(getMCQuestionList, new Random());
		
		if (!getQuestionsList.isEmpty()) {
			for (Question questionRow : getQuestionsList) {
				System.out.println("\n--------Question :------------");
				System.out.println(questionRow.getQuestion() + "\n");
				Answer answer = daoObj.searchAnswer(questionRow.getQuestionID());
				String inputAnswer = getResp(scanner, " answer :\n");
				
				if (inputAnswer.equalsIgnoreCase(answer.getAnswer())) {
					score += 1;
				}

			}
		}
		if (!getMCQuestionList.isEmpty()) {
			for (MCQ questionRow : getMCQuestionList) {
				System.out.println("\n--------Question :------------");
				System.out.println("\n" + questionRow.getQuestion());
				List<MCA> mcaList = daoObj.searchMCA(questionRow.getQuestionID());
				System.out.println("\n----------------------\nAnswer Choices:\n");
				List<Integer> validChoices = new ArrayList<>();
				int i = 0;
				for (MCA answerRow : mcaList) {
					System.out.println(++i + " " + answerRow.getAnswer() + "\n");
					if (answerRow.isCorrectOption()) {
						validChoices.add(i);
					}
				}
				List<Integer> studentChoices = new ArrayList<>();
				boolean exitChoice = false;
				do {
					int choiceNum = Integer.parseInt(getResp(scanner, " Valid answer choices one by one \n"));
					studentChoices.add(choiceNum);
					exitChoice = "Q".equalsIgnoreCase(
							getResp(scanner, "Press q to exit or any key to continue entering you choices !")) ? true
									: false;
				} while (!exitChoice);
				Collections.sort(studentChoices);

				if (studentChoices.equals(validChoices)) {
					score += 1;
					System.out.println(score);
				}
			}
		}
		return score;
	}

	private String getResp(Scanner scanner, String proposedQuestion) {
		System.out.println("Kindly enter the " + proposedQuestion+"\n");
		return scanner.nextLine();
	}

	/**
	 * Update Question method to update open and MC Questions.
	 * @param scanner
	 */
	public void updateQuestion(Scanner scanner) {
		int tableChoice = Integer.parseInt(getResp(scanner, "type of question: \n1.Open Question \n2.MCQ"));
		int questionID = Integer.parseInt(getResp(scanner,
				"Question ID to edit: \n Please note , you can get exact question id by keyword search"));
		String newQLable = getResp(scanner, "New question or nothing for no modification :");
		int newDiff = Integer.parseInt(getResp(scanner, " New difficulty or zero for no modification"));
		int newTopicID = Integer.parseInt(getResp(scanner, " TopicID: \n1.Java \n2.UML or zero for no modification"));
		Question searchObj;

		if (tableChoice == 1) {
			searchObj = new Question();
		} else {
			searchObj = new MCQ();
		}
		searchObj.setQuestionID(questionID);
		Question searchByIDRes = daoObj.searchByID(searchObj);
		if (!newQLable.isEmpty())
			searchByIDRes.setQuestion(newQLable);
		if (newDiff != 0)
			searchByIDRes.setDifficulty(newDiff);
		if (newTopicID != 0)
			searchByIDRes.setTopicID(newTopicID);
		daoObj.updateQuestion(searchByIDRes);

	}

	/**
	 * Deletion of a Question will be handled in this method. 
	 * @param scanner
	 */
	public void deleteQuestion(Scanner scanner) {
		int tableChoice = Integer.parseInt(getResp(scanner, "type of question: \n1.Open Question \n2.MCQ"));
		int questionID = Integer.parseInt(getResp(scanner,
				"Question ID to delete: \n Please note , you can get exact question id by keyword search"));
		Question searchObj;
		if (tableChoice == 1) {
			searchObj = new Question();
		} else {
			searchObj = new MCQ();
		}
		searchObj.setQuestionID(questionID);
		daoObj.deleteQuestionAns(searchObj);

	}
}